package com.parser;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class Main {
    static boolean wasRecord = false;

    public static void main(String[] args) throws IOException {
        File file = new File("заказы.xlsx");
        XSSFWorkbook myExcelBook = new XSSFWorkbook(new FileInputStream(file));
        XSSFSheet myExcelSheet = myExcelBook.getSheet("Лист1");
        Iterator<Row> rowIterator = myExcelSheet.rowIterator();
        printInformation(rowIterator);
    }

    private static void printInformation(Iterator<Row> rowIterator) {
        while (rowIterator.hasNext()){
            wasRecord = false;
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            Cell cell = cellIterator.next();
            String cellValue = getCellValue(cell);
            if(cellValue.matches("[А-Я][а-я]*\\s[А-Я][а-я]*") || cell.getColumnIndex() ==0){
                System.out.print("Заказчик: "+cellValue+ " ");
                while (cellIterator.hasNext()) {
                    cell = cellIterator.next();
                    cellValue = getCellValue(cell);
                    if (cellValue.replaceAll("\\D", "").matches("\\d{10}|\\d{11}")) {
                        System.out.print("Телефон - " + cellValue);
                    }
                }
                System.out.println("\nТовары:");
                continue;
            }
            do {
                if (cellValue.replaceAll("[^\\d,\\.]","").matches("\\d{3}\\.\\d{3}\\.\\d{2}")){
                    System.out.print("Артикул - "+ cellValue+ " ");
                    wasRecord =true;
                }
                if(cellValue.matches("[А-Я]{3,}(.|\\n|\\r)*")){
                    System.out.print("Наименование - "+cellValue.replaceAll("\n"," ").replaceAll("\r"," "));
                    wasRecord =true;
                }
                if((cellIterator.hasNext())) {
                    cell = cellIterator.next();
                    cellValue = getCellValue(cell);
                }
            } while (cellIterator.hasNext());
            if(wasRecord){
                System.out.println();
            }
        }
    }

    private static String getCellValue(Cell cell) {
        try {
            FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
            if (cell == null) return "";
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_BOOLEAN:
                    return String.valueOf(cell.getBooleanCellValue());
                case Cell.CELL_TYPE_NUMERIC:
                    return String.valueOf(cell.getNumericCellValue());
                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue();
                case Cell.CELL_TYPE_FORMULA:
                    return evaluator.evaluate(cell).getNumberValue()+"";
                default:
                    return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
